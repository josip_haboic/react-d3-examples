import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Nav } from './components/layout/nav'
import { Footer } from './components/layout/footer'
import { Charts } from './pages/charts'



function App() {
  return (
    <Router>
      <Nav></Nav>
      <main>
        <Route path="/" exact component={Charts}></Route>
      </main>
      <Footer />
    </Router>
  );
}

export default App;
