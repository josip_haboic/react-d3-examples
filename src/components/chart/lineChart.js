import React, { Component } from 'react'
import * as d3 from 'd3'
import '../../css/chart.css'



export class LineChart extends Component {

    constructor(props) {
        super(props)
        this.svgRef = React.createRef()
        this.xAxisRef = React.createRef()
        this.yAxisRef = React.createRef()
        this.gridXRef = React.createRef()
        this.gridYRef = React.createRef()
        this.dotsRef = React.createRef()
        this.pathRef = React.createRef()
    }

    get width() { 
        return this.props.width 
    }

    get fullWidth() {
        return this.props.width + this.margin.left + this.margin.right
    }

    get height() {
        return this.props.height 
    }

    get fullHeight() {
        return this.props.height + this.margin.top + this.margin.bottom
    }

    get margin() {
        return this.props.margin
    }

    get dotColor() {
        if (!this.props.dot) return "#ffab00"
        return this.props.dot.color ? this.props.dot.color : "#ffab00"
    }

    get lineColor() {
        if (!this.props.line) return "#ffab00"
        return this.props.line.color ? this.props.line.color : "#ffab00"
    }

    get data() {
        return this.props.data
    }

    get n() {
        return this.data.length
    }

    get xScale() {
        return d3.scaleLinear()
            .domain([d3.min(this.data.map((d) => d.x)), d3.max(this.data.map((d) => d.x))])
            .range([0, this.width])
    }

    get yScale() {
        return d3.scaleLinear()
            .domain([d3.min(this.data.map((d) => d.y)), d3.max(this.data.map((d) => d.y))])
            .range([this.height, 0])
    }

    get line() {
        return d3.line()
            .x((d) => this.xScale(d.x) + this.margin.left)
            .y((d) => this.yScale(d.y) + this.margin.top)
            .curve(d3.curveMonotoneX)
    }

    createXAxis() {
        d3.select(this.xAxisRef.current)
            .attr('class', 'x axis')
            .attr('transform', `translate(${this.margin.left}, ${this.height + this.margin.top})`)
            .call(d3.axisBottom(this.xScale)
                .ticks(this.n))
    }

    createYAxis() {
        d3.select(this.yAxisRef.current)
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)
            .attr('class', 'y axis')
            .call(d3.axisLeft(this.yScale)
                .ticks(this.n))
    }

    createPath() {
        d3.select(this.pathRef.current)
            .datum(this.data)
            .transition()
            .style('stroke', this.lineColor)
            .duration(700)
            .attr('class', 'line')
            .attr('d', this.line)
    }

    createDots() {
        return d3.select(this.dotsRef.current).selectAll('.dot')
            .data(this.data)
            .enter()
            .append('circle')
            .attr('class', 'dot')
            .attr('cx', (d, _i) => this.xScale(d.x) + this.margin.left)
            .attr('cy', (d, _i) => this.yScale(d.y) + this.margin.top)
            .attr('r', 5)
            .transition().style("fill", this.dotColor).duration(1000)
    }

    removeDots() {
        d3.select(this.dotsRef.current).selectAll('.dot').remove().exit()
    }

    createGrid() {
        const gridX = d3.select(this.gridXRef.current)
        gridX.attr('class', 'grid')
        gridX.attr('transform', `translate(${this.margin.left}, ${this.height + this.margin.top})`)
        gridX.style('stroke-dasharray', ('3, 3'))
        gridX.call(d3.axisBottom(this.xScale).ticks(this.n).tickSize(-this.height).tickFormat(''))

        const gridY = d3.select(this.gridYRef.current)
        gridY.attr('class', 'grid')
        gridY.attr('transform', `translate(${this.margin.top}, ${this.margin.left})`)
        gridY.style('stroke-dasharray', ('3, 3'))
        gridY.call(d3.axisLeft(this.yScale).ticks(this.n).tickSize(-this.width).tickFormat(''))

    }

    build() {
        const svg = d3.select(this.svgRef.current)
        svg.attr('viewBox', [0, 0, this.fullWidth, this.fullHeight])
        svg.attr('width', '100%')
        svg.attr('height', '100%')

        this.createXAxis()
        this.createYAxis()
        this.createGrid()
        this.updateGraph()
    }
    
    updateGraph() {
        this.createPath()
        if (this.props.dots) {
            this.removeDots()
            this.createDots()
        }
    }

    componentDidMount() {
        this.build()
    }
    componentDidUpdate() {
        this.build()
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.data === this.props.data) {
            return false
        }
        return true
    }
    render() {
        return (
            <svg ref={this.svgRef}>
                <g ref={this.xAxisRef}></g>
                <g ref={this.yAxisRef}></g>
                <g ref={this.gridXRef}></g>
                <g ref={this.gridYRef}></g>
                <g ref={this.dotsRef}></g>
                <path ref={this.pathRef}></path>
            </svg>
        )
    }

}