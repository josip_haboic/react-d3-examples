export const generateValues = (n=10, max=10) => {
    const values = []
    while (n !== 0) {
      values.push(Math.random() * max)
      n -= 1
    }
    return values.sort((a, b) => a >= b).map((x, i) => {
      return { x: i, y: x}
    }) 
  }