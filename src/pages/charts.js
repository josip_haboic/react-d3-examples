import React, { useState } from 'react'
import { LineChart } from '../components/chart/lineChart'
import { generateValues } from '../lib/generateData'



export const Charts = () => {
    const width = 400
    const height = 200

    
    const [data0n, setData0n] = useState(10)
    const [data0max, setData0max] = useState(100)

    const dataGenerator0 = () => generateValues(data0n, data0max)
    const [data0, setData0] = useState(dataGenerator0)

    return (
        <div className="container">

            <div className="charts">

                <div className="chart">
                    <LineChart
                        data={data0}
                        width={width}
                        height={height}
                        margin={{ top: 32, bottom: 32, left: 32, right: 32 }}
                        dots={true}
                    >
                    </LineChart>
                </div>
            </div>

            <div className="controls">
                <div className="control">
                    <label>Number of items</label>
                    <select onChange={(e) => setData0n(Number(e.target.value))}>
                        {[10, 20, 30].map((i) => {
                            return (
                                <option value={i} selected={i === data0n ? true : false}>{i}</option>
                            )
                        })}
                    </select>
                </div>
                <div className="control">
                    <label>Biggest number</label>
                    <select onChange={(e) => setData0max(e.target.value)}>
                        {[10, 50, 100, 500].map((i) => {
                            return (
                                <option value={i} selected={i === data0max ? true : false}>{i}</option>
                            )
                        })}
                    </select>
                </div>
                <div className="control">
                    <button
                        className="control button"
                        onClick={() => setData0(dataGenerator0)}>
                            Randomize
                    </button>
                </div>
            </div>
        </div>
    )
}